import React from 'react';
import LogoNetinflix from './components/LogoNetinflix';
import HeaderNetinflix from './components/HeaderNetinflix';
import ButtonNetinflix from './components/ButtonNetinflix';
import FooterNetinflix from './components/FooterNetinflix';
import LinkNetinflix from './components/LinkNetinflix';
import HighLightNetinflix from './components/HighLightNetinflix';

function App() {
  return (
    <>
      <HeaderNetinflix>
        <LogoNetinflix />

        <ButtonNetinflix>Novo vídeo</ButtonNetinflix>
      </HeaderNetinflix>

      <FooterNetinflix>
        <LogoNetinflix />
        <p>
          Site feito na <HighLightNetinflix>#ImersãoReact</HighLightNetinflix> da {' '}
          <LinkNetinflix href="https://alura.com.br">Alura</LinkNetinflix>
        </p>
      </FooterNetinflix>
    </>
  );
}

export default App;
