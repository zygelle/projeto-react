import styled from 'styled-components';
import logo from '../../assets/img/logo.png';

const LogoNetinflix = styled.img.attrs({ src: logo , alt: 'Logo da Net{IN}flix' })`
    height: 40px;
`;

export default LogoNetinflix;