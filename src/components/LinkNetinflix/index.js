import styled from 'styled-components';

const LinkNetinflix = styled.a`
    text-decoration: none;
    font-weight: bold;
    color: var(--color-primary-medium);
    cursor: pointer;
`;

export default LinkNetinflix;