import styled from 'styled-components';
import LogoNetinflix from '../LogoNetinflix';

const FooterNetinflix = styled.footer`
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: var(--color-black-dark);
    color: var(--color-gray-light);
    font-size: 16rem;
    padding: 20rem 0;
    border-top: 4px solid var(--color-primary-medium);

    & > ${LogoNetinflix} {
        margin-bottom: 20rem;
    }
`;

export default FooterNetinflix;