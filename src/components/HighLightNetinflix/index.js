import styled from 'styled-components';

const HighLightNetinflix = styled.strong`
    font-weight: bold;
`;

export default HighLightNetinflix;